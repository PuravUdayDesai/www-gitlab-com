---
layout: handbook-page-toc
title: MLOps Single-Engineer Group
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## MLOps Single-Engineer Group

MLOps is a [Single-Engineer Group](/company/team/structure/#single-engineer-groups) within our [Incubation Engineering Department](/handbook/engineering/incubation).

This group will be focused on enabling data teams to build, test, and deploy their machine learning models. This will be net new functionality within GitLab and will bridge the gap between DataOps teams and ML/AI within production. In addition, MLOps will provide tuning, testing, and deployment of machine learning models, including version control and partial rollout and rollback.

**Model**

Algorithm selection based on data shape and analytics doing parameter tuning, feature selection, and data selection. This includes git-focused functionality for hosting machine learning models. Examples include JupyterHub and Anaconda as well as DVC, Dolt, and Delta lake.

**Train**

Provide data teams with the tools to take raw large data sets, clean / morph / wrangle the data, and import the sanitized data into their models to prepare for deployment. Examples of this include Trifacta, TensorFlow Serving, UbiOps, and Sagemaker.

**Test**

Verify everything works as expected and is ready for deployment. Examples of this include PyTest, PyTorch, Keras, and Scikit-learn.

**Deploy**

Enable data teams to deploy their data models including partial rollout, partial rollback, and versioning of training data. Examples of this include Kubeflow and CML.

Our MLOps focus could also start with a focus on enabling partners to properly integrate into GitLab. Potential partners include Domino Data Lab, Determined AI, and Maiot.

## Issue Link

[https://gitlab.com/gitlab-org/gitlab/-/issues/329591](https://gitlab.com/gitlab-org/gitlab/-/issues/329591)

### Vacancy

We’re currently hiring for this role and looking for someone that understands the market, the opportunities, and the complexities to help design and develop our entry into MLOps. You’ll need experience in bringing products to markets, experience with Machine Learning tools, and experience developing and operating large-scale services. You should know the major competitors and partners in this space, and be able to architect capabilities around hosting machine learning models, training models, test and certification, and finally enable data teams to deploy with partial rollout and rollback and versioning. Our tech stack is Ruby, Go, and Vue.js, and you’ll need to work across backend, frontend database, and infrastructure to bring this opportunity to market.  

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/uO221nchszs" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

You can [apply directly](https://boards.greenhouse.io/gitlab/jobs/5106661002) or see our [careers page](https://about.gitlab.com/jobs/careers/) to learn about working at GitLab.

### Glossary

* **Experiment** A machine learning experiment is a set of variables that are varied across multiple runs in order to observe, and learn from, the effects of changes.

* **MLOps** is the process of taking an experimental Machine Learning model into a production web system

* **ModelOps** is a superset of ML/AI functionality including MLOps and DataOps. More information [here](/direction/modelops)


### Reading List

#### Internal Discussions
* [ModelOps Discussion w/ Board Members](https://docs.google.com/document/d/1ONYRRw7tSOpGcETiM2J_P-Df58MLi3PKGRXfl5iIYL8/edit)
* [GitLab ModelOps](https://docs.google.com/document/d/1jI1z3aT8kpF20vlxKeTteowVGfFJEh3BmnzG-cjtnZU/edit?ts=60538b97)
* [MLOps with Gitlab Paper](https://docs.google.com/document/d/1uMmB8gad7fe28aZCSBOIK6z9CbCtDmFV2ZUUuHH1YcY/edit#)
* [AI/ML Customers at Gitlab](https://docs.google.com/document/d/1QuR-kdFHYbb7HiBpJ36U3znd70juFkduVXVVY3YNd5A/edit)
* [https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB](https://datastudio.google.com/u/0/reporting/c708a721-2e95-409b-9a40-4c14eff1ae06/page/4OrWB)

#### Industry news
* [6 May 2021 - Introducing GitLab, Bitbucket Cloud, and Bitbucket Server source code management for Algorithmia](https://algorithmia.com/blog/introducing-gitlab-bitbucket-cloud-and-bitbucket-server-source-code-management-for-algorithmia)
