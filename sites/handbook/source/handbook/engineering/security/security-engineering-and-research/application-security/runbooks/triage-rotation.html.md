---
layout: handbook-page-toc
title: Triage Rotation
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Triage Rotation

Application Security team members may assign themselves as the directly
responsible individual (DRI) for incoming requests to the Application
Security team for a given calendar week in the [Triage Rotation](https://docs.google.com/spreadsheets/d/18vz84dgTfetTaBjbOCXaLKNfzLYMiy_tBW6RfEUYYHk/edit?ts=5ce48702#gid=0)
Google Sheet in the Security Team Drive.

The following rotations are defined:

- (Weekly Assignment) HackerOne + Mentions and Issues
    - One application security engineer is assigned this task each week and can be found at [Triage Rotation](https://docs.google.com/spreadsheets/d/18vz84dgTfetTaBjbOCXaLKNfzLYMiy_tBW6RfEUYYHk/edit?ts=5ce48702#gid=0).
    - Point of contact for "New" HackerOne reports during that week.
    - Responsible to escalating to other team members and management if the size of
    the either queue spikes.
    - First responder to mentions of the following group aliases:
        - @gitlab-com/gl-security/appsec on GitLab.com
        - @appsec-team in Slack
    - First responder to automated messages posted in the `#public_merge_requests_referencing_confidential_issues` Slack channel
    - Issues created needing triage: [~security-triage-appsec issue search](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=security-triage-appsec)
    - [Security Dashboards](./security-dashboard-review.html)
        - [GitLab](https://gitlab.com/gitlab-org/gitlab/-/security/vulnerability_report)
        - [GitLab Workhorse](https://gitlab.com/gitlab-org/gitlab-workhorse/-/security/vulnerability_report)
        - [Gitaly](https://gitlab.com/gitlab-org/gitaly/security/vulnerability_report)
        - [GitLab Pages](https://gitlab.com/gitlab-org/gitlab-pages/-/security/vulnerability_report)
        - [GitLab Shell](https://gitlab.com/gitlab-org/gitlab-shell/-/security/vulnerability_report)
        - [License](https://gitlab.com/gitlab-org/license-gitlab-com/-/security/vulnerability_report)
        - [k8s-workloads](https://gitlab.com/groups/gitlab-com/gl-infra/k8s-workloads/-/security/vulnerability_report)
        - [Version](https://gitlab.com/gitlab-services/version-gitlab-com/-/security/vulnerability_report)
        - [UBI images](https://gitlab.com/gitlab-com/gl-security/appsec/container-scanners/-/security/vulnerability_report/)
        - [release-cli](https://gitlab.com/gitlab-org/release-cli/-/security/vulnerability_report/)
        - [vscode-extension](https://gitlab.com/gitlab-org/gitlab-vscode-extension/-/security/vulnerability_report)
        - [Customers](https://gitlab.com/gitlab-org/customers-gitlab-com/-/security/vulnerability_report)
        - [elastic-search-indexer](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/security/vulnerability_report)
        - [GitLab DAST](https://gitlab.com/gitlab-org/security-products/dast/-/security/vulnerability_report/)
        - [GitLab Terminal](https://gitlab.com/gitlab-org/gitlab-terminal/-/security/vulnerability_report/)
        - [GitLab Agent](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/security/vulnerability_report)
        - [Compensation Calculator](https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/-/security/vulnerability_report)

- (Monthly Assignment) Security Engineer for Security Releases
- (Quarterly Assignment) Bug Bounty/AppSec Blog Post

Team members should not assign themselves on weeks they are responsible for the
scheduled security release.

Team members not assigned as the DRI for the week should continue to triage
reports when possible, especially to close duplicates or handle related reports
to those they have already triaged.

Team members remain responsible for their own assigned reports.
