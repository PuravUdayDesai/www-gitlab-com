<!--
Guidelines for the release post manager before submitting the issue:

- Give the issue a title: Release Post X.Y Retrospective
- Replace the release post MR link
- Use the correct month to the feedback date
- Assign milestone

You can remove this comment once you completed the items above.
-->

Release post MR: https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/1234

Please add your feedback by **YYYY-MM-DD**. Thank you!

**Note for release post team** (release post manager, technical advisor, TW lead and Messaging lead): For items you want to cover during the live retro, please add the :star: emoji.

***

Start your comment with an H2 heading which will be used as its title, and add
the appropriate icon the front of it to indicate which category it falls under:

- :thumbsup: What went well?
- :thumbsdown: What didn't go well?
- :bulb: What can we improve?
- :question: Items that are relevant for discussion but not a clear good or bad event to improve upon

Example:

```md
## :thumbsdown: Inconsistency of feature names capitalization
```

Create individual comments for every entry so that each item can have its own thread. Then, each thread will get an `**Action: @mention**` for resolution tracking.

## How to close this retro

- During and after release, add threads here
- Once the release is over, strive to close each thread with a :white_check_mark: emoji
- Each thread will be actioned with MRs where available or a resolution

/label ~"release post" ~"Product Retrospectives" ~"Product Operations"
